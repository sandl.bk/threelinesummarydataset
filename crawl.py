from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import logging
import pandas as pd
import json
from tqdm import tqdm
from time import sleep
import os

# chrome_options = Options()
# chrome_options.add_argument("--no-sandbox")
# chrome_options.add_argument("--disable-dev-shm-usage")
# chrome_options.add_argument("--headless")
# chrome_options.page_load_strategy = 'eager'

# driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')

def get_html(id, driver):
    url = f"https://news.livedoor.com/article/detail/{id}/"
    driver.get(url)
    return driver.page_source

def parse_html(id: str, driver):
    html = get_html(id, driver)
    check_null = 0
    try:
        soup = BeautifulSoup(html, 'html.parser')
        article = soup.find('div', {'id':"article-body"})
    except:
        print(f'html null return in {id}')
        article = None
        check_null = 1
    article_title, article_body, summary = '', '', None
    if article is not None:
        # logger.debug(f'ID: {id}: Successfully')
        article_title = article.find('h1', {'class':"articleTtl"}).getText()
        article_body = article.find('span', {'itemprop':"articleBody"})
        article_body = article_body.find_all('p')
        article_body = [b.getText() for b in article_body]
        article_body = '\n'.join(article_body)
        
        summary = soup.find('ul', {'class':"summaryList"})
        summary = summary.find_all('li')
        summary = [s.getText() for s in summary]
    else:
        # logger.debug(f'ID: {id}: Failed')
        # print(html)
        summary = []
    return article_title, article_body, summary, check_null


def build_json(list_json):
    chrome_options = Options()
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--headless")
    chrome_options.page_load_strategy = 'eager'

    driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
    if len(list_json) == 0:
        sleep(2)
        return
    for j in list_json:
        build_a_json(j, driver)


def build_a_json(file_json, driver):
    id = file_json.split('/')[-1][:-5]
    article_title, article_body, summary, check_null = parse_html(id, driver)
    d = {'id': id, 'article_title': article_title, 'article_body': article_body, 'summary': summary}
    if check_null == 0:
        with open(file_json, mode="w", encoding='utf8') as write_file:
            print(f'id {id} ----- article_title: {article_title}')
            json.dump(d, write_file, ensure_ascii=False)
    sleep(1)
