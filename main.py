from multiprocessing import Process
import os
import sys
import numpy as np
import multiprocessing
from crawl import build_a_json, build_json
import pandas as pd


a = list(np.arange(1000))
# b = list(np.arange(1000))
def info(title):
    print(title)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())


from os import listdir
from os.path import isfile, join

def get_existed_file(mypath):
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    return onlyfiles
    

def get_list_file(mode):
    csv = pd.read_csv(f'./data/{mode}.csv', header=None)[3].to_list()
    csv = [f'./crawl_data/{mode}/{id}.json' for id in csv]
    # print(csv)
    # exit(0)
    return csv

def split_data(csv, num_div):
    res = []
    for i in range(num_div):
        tmp = [csv[j] for j in range(len(csv)) if j % num_div == i]
        res.append(tmp)
    return res
        
if __name__ == '__main__':
    arg = sys.argv[1]
    max_processor = 4
    info('main line')
    csv = get_list_file(arg)
    new_csv = []
    # print(csv[:10])
    existed_file = set(get_existed_file('./crawl_data/' + arg))
    for i in csv:
        tmp = i.split('/')[-1]
        if tmp not in existed_file:
            new_csv.append(i)
    print('the number remaining file: ' + str(len(new_csv)))
    # csv = new_csv[:8]
    csv = split_data(csv, max_processor)

    all_process = []
    for i in range(max_processor):
        p = Process(target=build_json, args=(csv[i],))
        all_process.append(p)
        p.start()
    for p in all_process:
        p.join()



