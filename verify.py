from main import *
from crawl import *
import json


def get_list_null_file(arg):
    dir = './crawl_data/' + arg
    lst_file = get_existed_file(dir)
    lst_file = [dir + '/' + i for i in lst_file]
    csv = []
    for fi in lst_file:
        with open(fi, encoding='utf-8') as f:
            tmp_data = json.load(f)
            if tmp_data['article_title'] == '':
                csv.append(fi)
    return csv


if __name__ == '__main__':
    arg = sys.argv[1]
    csv = get_list_null_file(arg)
    print('the number null file: ' + str(len(csv)))