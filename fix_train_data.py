import re
import pandas as pd

df = pd.read_csv(f'./data/train.csv', header=None)
print(df)
train = df[3].to_list()

for i in range(len(train)):
    tmp = train[i]
    tmp = re.sub("\D+", "", tmp, count=1)
    train[i] = tmp

df[3] = train

df.to_csv('./data/train.csv', header=None, index=False)



